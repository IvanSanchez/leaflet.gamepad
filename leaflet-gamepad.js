// Leaflet.Gamepad, by Iván Sánchez Ortega <ivan@sanchezortega.es>
// Inspired off some bits of https://github.com/luser/gamepadtest/

// I haven't decided on a license for this, so please don't redistribute for the time being
// (or drop me an email asking politely about this)




// @namespace Map
// @section Interaction Options
L.Map.mergeOptions({
	// @option gamepad: Boolean = true
	// Whether the map can be panned/zoomed with a gamepad (or joystick).
	// The map must have focus in order to be controlled by a gamepad, so this actually
	// depends on the "keyboard" handler being active (which should be default).
	gamepad: true,

	// @option gamepadAxisX: Number = 0
	// Which axis of gamepads shall control map pan across the X axis (horizontal, longitude).
	// Setting is shared across any and all gamepads connected to the system.
	// Per-gamepad settings (or per-gamepad-model settings) are not implemented yet.
	gamepadAxisX: 0,

	// @option gamepadAxisY: Number = 1
	// Idem of `gamepadAxisX`, but for the Y axis (vertical, latitude)
	gamepadAxisY: 1,

	// @option gamepadButtonZoomIn: Number = 0
	// Which button of gamepads shall zoom the map in (increase the zoom level of the map) when pressed.
	// This shall zoom in by the map's `zoomDelta` option
	// Same restrictions than `gamepadAxisX` in regards to per-gamepad config also apply.
	gamepadButtonZoomIn: 0,

	// @option gamepadButtonZoomOut: Number = 1
	// Idem of `gamepadButtonZoomIn`, but for zooming out.
	gamepadButtonZoomOut: 1,

	// @option gamepadPanSpeed: Number = 600
	// How fast the map should pan when an axis is pushed (all the way to a limit position)
	// The magnitude is in CSS pixels per second
	gamepadPanSpeed: 600,
});


L.Handler.Gamepad = L.Handler.extend({
	initialize: function(map){
		this._map = map;
		this._polling = false;
		this._lastPollTimestamp = performance.now();
	},
	addHooks: function(){
		this._map.on('focus', this._pollGamepads, this);
		this._map.on('blur', this._unpollGamepads, this);
		if (this._map.keyboard && this._map.keyboard._focused && !this._polling) {
			this._pollGamepads();
		}
	},
	removeHooks: function(){
		this._unpollGamepads();
		this._map.on('focus', this._pollGamepads, this);
		this._map.on('blur', this._unpollGamepads, this);
	},

	_pollGamepads(){
		this._polling = true;
		this._onAnimFrame();
	},
	_unpollGamepads(){
		this._polling = false;
	},

	_onAnimFrame: function(){
		var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
		var opts = this._map.options;
		var panX = 0;
		var panY = 0;
		var zoomIn = false;
		var zoomOut = false;

		for (var i=0; i<gamepads.length; i++) {
			var pad = gamepads[i];
			if (pad) {
				if (pad.buttons[opts.gamepadButtonZoomIn].pressed) {
					zoomIn = true;
				}
				if (pad.buttons[opts.gamepadButtonZoomOut].pressed) {
					zoomOut = true;
				}
				panX += pad.axes[opts.gamepadAxisX];
				panY += pad.axes[opts.gamepadAxisY];
			}
		}

		if (panX || panY) {
			var timeOffset = performance.now() - this._lastPollTimestamp;
			if (timeOffset) {
				var speed = opts.gamepadPanSpeed * timeOffset / 1000;
				var offset = L.point(panX * speed, panY * speed);
				this._map.panBy(offset, {animate: false});
			}
		}
		if (zoomIn && zoomOut) {
			// noop
		} else if (zoomIn) {
			this._map.zoomIn();
		} else if (zoomOut) {
			this._map.zoomOut();
		}

		this._lastPollTimestamp = performance.now();
		if (this._polling) {
			L.Util.requestAnimFrame(this._onAnimFrame, this);
		}
	},
});


// @section Handlers
// @section Handlers
// @property gamepad: Handler
// Gamepad navigation handler.
L.Map.addInitHook('addHandler', 'gamepad', L.Handler.Gamepad);
